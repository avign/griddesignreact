import React, { Component } from "react";
import "./App.css";
import Filters from "./components/filters/Filters";
import Blog from "./components/Blog/Blog";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      blogs: [],
    };
  }

  formatBlogs(blogs) {
    const formatted_blogs = blogs.map((blog) => {
      return {
        id: blog.id,
        img_url: blog.small_image,
        header: blog.title,
        author: blog.authorname,
        main_content: blog.description,
        date: blog.posted_on,
      };
    });
    return formatted_blogs;
  }

  setBlogs(url) {
    fetch(url)
      .then((res) => res.json())
      .then((res) => {
        const blogs = res.posts ? res.posts : res;
        this.setState({ blogs: this.formatBlogs(blogs) });
      });
  }
  componentDidMount() {
    this.setBlogs("https://api.edyoda.com/v1/blog/");
  }

  handleFilterClick = (filter) => {
    if (filter.title === "All") {
      this.setBlogs("https://api.edyoda.com/v1/blog/");
    } else {
      this.setBlogs(`https://api.edyoda.com/v1/blog/${filter.slug}/`);
    }
  };

  render() {
    return (
      <div className="App">
        <Filters onFilterClick={this.handleFilterClick}></Filters>
        <div className="blogs">
          {this.state.blogs.map((blog) => (
            <Blog key={blog.id} {...blog} />
          ))}
        </div>
      </div>
    );
  }
}

export default App;
