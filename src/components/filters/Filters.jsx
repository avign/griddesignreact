import React, { Component } from "react";
import "./Filters.css";
import filter from "../../assets/images/filter.png";
import Badge from "../badge/badge";

class Filters extends Component {
  state = { filters: [], current_filter_id: -1 };
  componentDidMount() {
    fetch("https://api.edyoda.com/v1/blog/postcategories/")
      .then((res) => res.json())
      .then((res) =>
        this.setState({ filters: [{ id: -1, title: "All" }, ...res] })
      );
  }

  handleFilterClick = (filter_title) => {
    const clicked_filter = this.state.filters.find(
      (filter) => filter.title === filter_title
    );
    if (clicked_filter.id !== this.state.current_filter_id) {
      this.setState({ current_filter_id: clicked_filter.id });
      this.props.onFilterClick(clicked_filter);
    }
  };

  render() {
    return (
      <div className="filters">
        <div className="filters--header">
          <img className="filters--header--icon" src={filter}></img>
          <div className="filters--header--text">Filter By Category</div>
        </div>
        <div className="filters--container">
          {this.state.filters.map((filter) => (
            <Badge
              key={filter.id}
              badge_text={filter.title}
              is_active={filter.id === this.state.current_filter_id}
              onBadgeClick={this.handleFilterClick}
            />
          ))}
        </div>
      </div>
    );
  }
}

export default Filters;
