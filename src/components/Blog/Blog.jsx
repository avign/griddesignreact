import React from "react";
import "./Blog.css";

const BLog = (props) => {
  console.log(props);
  const { img_url, header, date, main_content, author } = props;
  return (
    <div className="blog">
      <div className="blog--image--container">
        <img src={img_url} />
      </div>
      <div className="blog--content">
        <div className="blog--content--header">{header}</div>
        <div className="blog--content--sub-header">
          <div className="blog--content--author">{author}</div>
          {/* <div className="blog--content--date">{`| ${date}`}</div> */}
        </div>
        <div className="blog--content--main-content">{main_content}</div>
      </div>
    </div>
  );
};

export default BLog;
