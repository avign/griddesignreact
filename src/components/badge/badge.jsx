import React, { Component } from "react";
import "./badge.css";

const Badge = ({ badge_text, onBadgeClick, is_active }) => {
  return (
    <div
      onClick={() => onBadgeClick(badge_text)}
      className={`badge ${is_active ? "active" : ""}`}
    >
      {badge_text}
    </div>
  );
};

export default Badge;
